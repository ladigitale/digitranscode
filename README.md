# Digitranscode

Digitranscode est une application simple pour convertir des fichiers audio et des vidéos grâce à FFMPEG.wasm - https://github.com/ffmpegwasm/ffmpeg.wasm (MIT License). 

Elle est publiée sous licence GNU AGPLv3.
Sauf la fonte Material Icons (Apache License Version 2.0) et la fonte Mona Sans Expanded (Sil Open Font Licence 1.1)

### Préparation et installation des dépendances
```
npm install
```

### Lancement du serveur de développement
```
npm run dev
```

### Compilation et minification des fichiers
```
npm run build
```

### Démo
https://ladigitale.dev/digitranscode/

### Soutien
Open Collective : https://opencollective.com/ladigitale

Liberapay : https://liberapay.com/ladigitale/

